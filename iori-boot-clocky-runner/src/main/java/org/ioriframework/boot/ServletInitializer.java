package org.ioriframework.boot;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 打成war包在tomcat中运行时，需要此类
 * 
 * @author iori
 *
 */
public class ServletInitializer extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		// 打WAR包在tomcat中启动时，设置默认的参数
//		Map<String, Object> defaultMap = new HashMap<String, Object>();
//		defaultMap.put("spring.config.location", "classpath:/config/exceptionMsg.yml");
//		application.properties(defaultMap);
		return application.sources(MainApplication.class);
	}
}
