package org.ioriframework.boot;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * 
 * spring boot 启动入口
 * 
 * @author iori
 *
 */
@SpringBootApplication(scanBasePackages={"org.ioriframework.boot"})
@ServletComponentScan({"org.ioriframework.boot"})
public class MainApplication {
	

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(MainApplication.class);
//		// jar包启动时，设置默认的参数
//		Map<String, Object> defaultMap = new HashMap<String, Object>();
//		defaultMap.put("spring.config.location", "classpath:/config/exceptionMsg.yml");
//		//还可以是Properties对象
//		application.setDefaultProperties(defaultMap);
		application.run(args);
	}
	
	/**
	 * 修改DispatcherServlet默认配置,默认UrlMapping为"/*"
	 * @param dispatcherServlet
	 * @return
	 */
	@Bean
    public ServletRegistrationBean<DispatcherServlet> dispatcherRegistration(DispatcherServlet dispatcherServlet) {
        ServletRegistrationBean<DispatcherServlet> registration = new ServletRegistrationBean<DispatcherServlet>(dispatcherServlet);
        registration.setUrlMappings(Arrays.asList("/services/*","/publicservices/*","/internalservices/*"));
        return registration;
    }

}
