package org.ioriframework.boot.job.task;

import org.ioriframework.boot.core.util.IoriContextUtils;
import org.ioriframework.boot.job.vo.JobVO;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Rest服务定时任务(并发执行)
 * 此类型的任务应该是一个Rest服务，处理器根据任务配置，定时调度相应Rest服务
 * @author iori
 *
 */
public class RestJobTask implements IJobTask {

	private static Logger log = LoggerFactory.getLogger(RestJobTask.class);
	
	@Override
	public void process(JobVO jobVO) {
		RestJobProcesser restJobProcesser = IoriContextUtils.getBean("restJobProcesser", RestJobProcesser.class);
		restJobProcesser.process(jobVO);
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.debug("web service job execute...");
		JobVO jobVO = (JobVO) context.getMergedJobDataMap().get("jobVO");
		process(jobVO);
	}

}
