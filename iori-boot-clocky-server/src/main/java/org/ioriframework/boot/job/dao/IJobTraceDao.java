package org.ioriframework.boot.job.dao;

import org.apache.ibatis.annotations.Param;
import org.ioriframework.boot.core.base.vo.PageVO;
import org.ioriframework.boot.job.vo.JobTraceVO;

public interface IJobTraceDao {

	/**
	 * 查询Job执行记录列表
	 * @param jobTraceVO
	 * @param pageVO
	 * @return
	 */
	public PageVO<JobTraceVO> findJobTraceList(@Param("jobTraceVO") JobTraceVO jobTraceVO, @Param("pageVO") PageVO<JobTraceVO> pageVO);
	
	/**
	 * 创建Job执行记录
	 * @param jobTraceVO
	 */
	public void createJobTrace(JobTraceVO jobTraceVO);
	
}
