package org.ioriframework.boot.job.vo;

import org.ioriframework.boot.core.base.vo.BaseResourceVO;

/**
 * 任务执行记录VO
 * @author iori
 *
 */
public class JobTraceVO extends BaseResourceVO {

	private static final long serialVersionUID = 49862271257724195L;

	/**
	 * 任务执行记录ID
	 */
	private Integer jobTraceId;
	
	/**
	 * 任务ID
	 */
	private Integer jobId;
	
	/**
	 * 运行用时(单位:毫秒)
	 */
	private Long totalTime;
	
	/**
	 * 任务返回值
	 */
	private String returnValue;
	
	/**
	 * 最终状态, 0:失败,1:成功
	 */
	private Integer finalStatus;

	public Integer getJobTraceId() {
		return jobTraceId;
	}

	public void setJobTraceId(Integer jobTraceId) {
		this.jobTraceId = jobTraceId;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public Long getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(Long totalTime) {
		this.totalTime = totalTime;
	}

	public String getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(String returnValue) {
		this.returnValue = returnValue;
	}

	public Integer getFinalStatus() {
		return finalStatus;
	}

	public void setFinalStatus(Integer finalStatus) {
		this.finalStatus = finalStatus;
	}

}
