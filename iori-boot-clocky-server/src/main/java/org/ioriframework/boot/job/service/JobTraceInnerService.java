package org.ioriframework.boot.job.service;

import org.ioriframework.boot.job.dao.IJobTraceDao;
import org.ioriframework.boot.job.vo.JobTraceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class JobTraceInnerService {
	
	@Autowired
	private IJobTraceDao jobTraceDao;


	/**
	 * 创建Job执行记录
	 * @param jobTraceVO
	 */
	@Transactional(transactionManager="txManager")
	public void createJobTrace(JobTraceVO jobTraceVO) {
		jobTraceDao.createJobTrace(jobTraceVO);;
	}

}
