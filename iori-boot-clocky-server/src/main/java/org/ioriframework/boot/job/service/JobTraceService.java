package org.ioriframework.boot.job.service;

import org.ioriframework.boot.core.base.vo.PageVO;
import org.ioriframework.boot.job.dao.IJobTraceDao;
import org.ioriframework.boot.job.vo.JobTraceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="job/trace")
public class JobTraceService {
	
	@Autowired
	private IJobTraceDao jobTraceDao;

	/**
	 * 查询JOB列表
	 * @param jobVO
	 * @param pageVO
	 * @return
	 */
	@GetMapping(value="/list", produces = "application/json; charset=UTF-8")
	public PageVO<JobTraceVO> findJobList(JobTraceVO jobTraceVO, PageVO<JobTraceVO> pageVO) {
		pageVO = jobTraceDao.findJobTraceList(jobTraceVO, pageVO);
		return pageVO;
	}

}
