package org.ioriframework.boot.job.task;

import org.ioriframework.boot.job.vo.JobVO;
import org.quartz.Job;

/**
 * 定时任务接口
 * 同一任务会并发执行，不会等待上一次执行完毕，只要间隔时间到就会执行下一次
 * 如果任务执行时间大于任务的时间间隔，会马上启用新的线程执行任务，任务并发执行，需要任务处理逻辑上注意线程安全和数据一致性问题
 * 如果定时任执行太长，会长时间占用资源，导致其它任务堵塞
 * 
 * 如需同一任务顺序执行，请使用{@link IStatefulJobTask}
 * @author iori
 *
 */
public interface IJobTask extends Job {

	/**
	 * 处理任务
	 */
	public void process(JobVO jobVO);
}
