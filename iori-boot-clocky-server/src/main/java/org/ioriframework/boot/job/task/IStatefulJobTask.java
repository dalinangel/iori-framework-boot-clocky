package org.ioriframework.boot.job.task;

import org.ioriframework.boot.job.vo.JobVO;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.PersistJobDataAfterExecution;

/**
 * 有状态的定时任务接口
 * 等待上一次任务执行完毕后再执行下一次任务
 * 
 * 如需同一任务并发执行，请使用{@link IJobTask}
 * @author iori
 *
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public interface IStatefulJobTask extends Job {

	/**
	 * 处理任务
	 */
	public void process(JobVO jobVO);
}
