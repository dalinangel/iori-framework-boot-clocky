package org.ioriframework.boot.job.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.ioriframework.boot.core.base.vo.PageVO;
import org.ioriframework.boot.job.vo.JobVO;

public interface IJobDao {

	/**
	 * 查询Job列表
	 * @param jobVO
	 * @param pageVO
	 * @return
	 */
	public PageVO<JobVO> findJobList(@Param("jobVO") JobVO jobVO, @Param("pageVO") PageVO<JobVO> pageVO);
	
	/**
	 * 创建Job
	 * @param JobVO
	 */
	public void createJob(JobVO jobVO);
	
	/**
	 * 删除Job
	 * @param JobId
	 */
	public void deleteJob(Integer jobId);
	
	/**
	 * 更新Job信息
	 * @param JobVO
	 */
	public void updateJob(JobVO jobVO);
	
	/**
	 * 更新Job状态
	 * @param JobVO
	 */
	public void updateJobStatus(JobVO jobVO);
	
	/**
	 * 通过JobID查询Job
	 * @param JobId
	 * @return
	 */
	public JobVO findJobById(Integer jobId);

	/**
	 * 查询Job
	 * @param jobVO
	 * @return
	 */
	public JobVO findJob(JobVO jobVO);
	
	/**
	 * 查询需要恢复的Job列表（过滤掉了失效状态的任务）
	 * @return
	 */
	public List<JobVO> findJobList4Resume();

	/**
	 * 根据Group查询Job
	 * @param jobVO
	 * @return
	 */
	public List<JobVO> findJobByGroup(JobVO jobVO);

	/**
	 * 根据Group删除Job
	 * @param jobVO
	 */
	public void deleteJobByGroup(JobVO jobVO);

	/**
	 * 根据Group更新Job状态
	 * @param jobVO
	 */
	public void updateJobStatusByGroup(JobVO jobVO);
}
