package org.ioriframework.boot.job.vo;

import org.ioriframework.boot.core.base.vo.BaseResourceVO;

/**
 * 任务VO
 * @author iori
 *
 */
public class JobVO extends BaseResourceVO {

	private static final long serialVersionUID = 9015487096225288434L;

	/**
	 * 任务ID
	 */
	private Integer jobId;
	
	/**
	 * 任务名称
	 */
	private String jobName;
	
	/**
	 * 任务类型，如WS：WEB服务
	 */
	private String jobType;
	
	/**
	 * 顺序执行标识，Y:顺序执行，N:并发执行
	 */
	private String statefulFlag;
	
	/**
	 * 任务组
	 */
	private String groupName;
	
	/**
	 * cron表达式
	 */
	private String cron;
	
	/**
	 * 任务内容，当类型为WS时，内容为服务URL
	 */
	private String jobContent;
	
	/**
	 * 任务状态，0:失效,1:运行中, 2:暂停
	 */
	private Integer status;

	/**
	 * 外围系统业务反写ID
	 */
	private Long backId;
	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public String getJobContent() {
		return jobContent;
	}

	public void setJobContent(String jobContent) {
		this.jobContent = jobContent;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatefulFlag() {
		return statefulFlag;
	}

	public void setStatefulFlag(String statefulFlag) {
		this.statefulFlag = statefulFlag;
	}

	public Long getBackId() {
		return backId;
	}

	public void setBackId(Long backId) {
		this.backId = backId;
	}
}
