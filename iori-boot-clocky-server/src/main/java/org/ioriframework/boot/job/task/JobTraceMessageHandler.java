package org.ioriframework.boot.job.task;

import org.ioriframework.boot.async.handler.IMessageHandler;
import org.ioriframework.boot.async.vo.AsyncMessageVO;
import org.ioriframework.boot.job.service.JobTraceInnerService;
import org.ioriframework.boot.job.vo.JobTraceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("async.jobTraceMessage")
public class JobTraceMessageHandler implements IMessageHandler {
	
	@Autowired
	private JobTraceInnerService jobTraceInnerService;

	@Override
	public boolean handle(AsyncMessageVO messageVO) throws Exception {
		JobTraceVO jobTraceVO = (JobTraceVO) messageVO.getContent();
		// 写JOB执行记录
		jobTraceInnerService.createJobTrace(jobTraceVO);
		return true;
	}

}
