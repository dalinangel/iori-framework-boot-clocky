Iori.Module.define("Iori.Job.Edit", function(page, $S) {
	
	page.ready = function() {
		var jobId = Iori.jobId;
		// 清空全局变量
		delete Iori.jobId;
		page.findJob(jobId);
	}
	
	page.findJob = function(jobId) {
		Iori.doGet($globalVO.config.bgApp.Clocky + "/services/job/find/" + jobId,
			null,
			function(data) {
				if (data) {
					// 填充form
					$("#editJobForm").fillForm(data);
				}
			}
		);
	}
	
	page.submit = function() {
		var formObj = $("#editJobForm");
		if (formObj.ioriValidate()) {
			Iori.doPut($globalVO.config.bgApp.Clocky + "/services/job/update",
				formObj.formData(),
				function(data) {
					// 关闭创建Job窗口
					Iori.UI.Modal.close();
					Iori.UI.showTip($i18n("iori.job.label.editSuccess"), "success");
					// 重新查询Job列表
					Iori.Job.List.searchJob();
				}
			);
		}
	}
	
});
