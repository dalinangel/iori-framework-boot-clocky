Iori.Module.define("Iori.Job.Common", function(page, $S) {
	
	page.ready = function() {
		page.buildCronList();
	}
	
	page.buildCronList = function() {
		Iori.doGet($globalVO.config.bgApp.Clocky + "/services/lookup/item/find/jobCornHint",null,function(data) {
			if (data && data.lookupItemList) {
				var cronList = $S("#cronList");
				var cronHelp = $S("#cronHelp");
				$.each(data.lookupItemList,function(i,obj) {
					cronList.append("<option value='" + obj.itemCode + "' label='" + obj.itemName + "'/>");
					cronHelp.append("<li role='presentation'><a role='menuitem' tabindex='-1' onclick='Iori.Job.Common.setCron(\"" + obj.itemCode + "\")'>" + obj.itemCode + "(" + obj.itemName + ")</a></li>");
				});
			}
		}); 
	}
	
	page.setCron = function(cron) {
		$S("#cron").val(cron);
	}
	
});


