Iori.Module.define("Iori.Job.Trace.List", function(page, $S) {
	
	page.ready = function() {
		var jobId = Iori.jobId;
		// 清空全局变量
		delete Iori.jobId;
		// 渲染Grid
		var grid = $("#jobTraceTable").ioriGrid({
			columns : [{
				id : "jobTraceId",
				text : "iori.job.label.jobId",
				converter : "numeric",
				// 默认不展示
				visible : false,
				// 不可以手动勾选展示
				visibleInSelection : false
			}, {
				id : "creationDate",
				text : "iori.common.label.creationDate",
				formatter : "dateTime"
			}, {
				id : "totalTime",
				text : "iori.job.label.totalTime",
				formatter : function(column, row) {
					// 转为秒来显示
					return row[column.id]/1000;
				}
			}, {
				id : "finalStatus",
				text : "iori.common.label.status",
				formatter : function(column, row) {
					if (row[column.id] == 1) {
						return "success";
					} else {
						return "fail";
					}
				}
			}, {
				id : "returnValue",
				text : "iori.job.label.returnValue",
				// 不支持排序
				sortable : false,
				formatter : "title"
			}],
			url: function() {
				return $globalVO.config.bgApp.Clocky + "/services/job/trace/list?jobId=" + jobId;
			},
			// 开启多列排序
			multiSort : true
		});
	}
	
});

