Iori.Module.define("Iori.Job.List", function(page, $S) {
	
	page.ready = function() {
		// 渲染Grid
		var grid = $("#jobTable").ioriGrid({
			columns : [{
				text : "iori.common.button.operation",
				formatter : function(column, row) {
					return "<button type=\"button\" title=\"edit\" class=\"btn-table command-edit init\" data-row-id=\"" + row.jobId + "\"><i class=\"iconfont gk_iconfont\">&#xe608;</i></button> "
						+ "<button type=\"button\" title=\"delete\" class=\"btn-table command-delete init\" data-row-id=\"" + row.jobId + "\"><i class=\"iconfont gk_iconfont\">&#xe611;</i></button> "
						+ "<button type=\"button\" title=\"pause\" class=\"btn-table command-pause init\" data-row-id=\"" + row.jobId + "\"><i class=\"iconfont gk_iconfont\">&#xe62f;</i></button> "
						+ "<button type=\"button\" title=\"resume\" class=\"btn-table command-resume init\" data-row-id=\"" + row.jobId + "\"><i class=\"iconfont gk_iconfont\">&#xe631;</i></button> "
						+ "<button type=\"button\" title=\"trigger once\" class=\"btn-table command-trigger init\" data-row-id=\"" + row.jobId + "\"><i class=\"iconfont gk_iconfont\">&#xe630;</i></button> "
						+ "<button type=\"button\" title=\"view job trace\" class=\"btn-table command-trace init\" data-row-id=\"" + row.jobId + "\"><i class=\"iconfont gk_iconfont\">&#xe617;</i></button> ";
				},
				// 不支持排序
				sortable : false,
				width : 200
			}, {
				id : "jobId",
				text : "iori.job.label.jobId",
				converter : "numeric",
				// 默认不展示
				visible : false,
				// 不可以手动勾选展示
				visibleInSelection : false
			}, {
				id : "jobName",
				text : "iori.job.label.jobName",
				width : 260
			}, {
				id : "jobType",
				text : "iori.job.label.jobType",
				// 不支持排序
				sortable : false,
				// 默认不展示
				visible : false
			}, {
				id : "groupName",
				text : "iori.job.label.group",
				width : 200
			}, {
				id : "status",
				text : "iori.common.label.status",
				width : 100,
				formatter : function(column, row) {
					if (row[column.id] == 2) {
						return $i18n("iori.job.label.pause");
					} else if (row[column.id] == 1) {
						return $i18n("iori.job.label.running");
					} else {
						return $i18n("iori.job.label.invalid");
					}
				}
			}, {
				id : "jobContent",
				text : "iori.job.label.content",
				// 不支持排序
				sortable : false,
				formatter : "title"
			}, {
				id : "cron",
				text : "iori.job.label.cron",
				// 不支持排序
				sortable : false,
				// 默认不展示
				visible : false
			}, {
				id : "statefulFlag",
				text : "iori.job.label.stateful",
				formatter : function(column, row) {
					return row[column.id] == "Y" ? $i18n("iori.job.label.stateful.y") : $i18n("iori.job.label.stateful.n");
				},
				// 默认不展示
				visible : false
			}, {
				id : "createdBy",
				text : "iori.common.label.createdBy",
				// 不支持排序
				sortable : false,
				// 默认不展示
				visible : false
			}, {
				id : "creationDate",
				text : "iori.common.label.creationDate",
				formatter : "dateTime",
				// 不支持排序
				sortable : false,
				// 默认不展示
				visible : false
			}, {
				id : "lastUpdatedBy",
				text : "iori.common.label.lastUpdatedBy",
				// 不支持排序
				sortable : false,
				// 默认不展示
				visible : false
			}, {
				id : "lastUpdateDate",
				text : "iori.common.label.lastUpdateDate",
				formatter : "dateTime",
				// 默认不展示
				visible : false
			}],
			url: $globalVO.config.bgApp.Clocky + "/services/job/list",
//			url: "services/job/list",
			// 开启多列排序
			multiSort : true,
			// 绑定loaded事件(grid加载完成后执行)
			loaded : function(grid) {
				grid.find(".command-edit.init").on("click", function(e) {
					Iori.Job.List.editJob($(this).data("row-id"));
				}).removeClass("init");
				grid.find(".command-delete.init").on("click", function(e) {
					Iori.Job.List.deleteJob($(this).data("row-id"));
				}).removeClass("init");
				grid.find(".command-pause.init").on("click", function(e) {
					Iori.Job.List.pauseJob($(this).data("row-id"));
				}).removeClass("init");
				grid.find(".command-resume.init").on("click", function(e) {
					Iori.Job.List.resumeJob($(this).data("row-id"));
				}).removeClass("init");
				grid.find(".command-trigger.init").on("click", function(e) {
					Iori.Job.List.triggerJob($(this).data("row-id"));
				}).removeClass("init");
				grid.find(".command-trace.init").on("click", function(e) {
					Iori.Job.List.viewJobTrace($(this).data("row-id"));
				}).removeClass("init");
			}
		});
	}
	
	// 新增Job
	page.addJob = function() {
		Iori.UI.Modal.open("ioriframework/boot/job/createJob.html",null,{"css" : "width:600px"});
	}
	
	// 删除Job
	page.deleteJob = function(jobId) {
		Iori.UI.confirm($i18n("iori.common.label.deleteConfirm"), function(flag) {
			if(flag == true) {
				Iori.doDelete($globalVO.config.bgApp.Clocky + "/services/job/delete/" + jobId,
					null,
					function(data) {
						$("#jobTable").ioriGridReload();
						Iori.UI.showTip($i18n("iori.common.label.deleteSuccess"), "success");
					}
			);
			}
		});
	}
	
	// 编辑Job
	page.editJob = function(jobId) {
		// 通过全局变量来传递参数
		Iori.jobId = jobId;
		Iori.UI.Modal.open("ioriframework/boot/job/editJob.html",null,{"css" : "width:600px"});
	}
	
	page.searchJob = function() {
		$("#jobTable").ioriGridReload();
	}
	
	// pause Job
	page.pauseJob = function(jobId) {
		Iori.UI.confirm($i18n("iori.job.label.pauseConfirm"), function(flag) {
			if(flag == true) {
				Iori.doPost($globalVO.config.bgApp.Clocky + "/services/job/pause/" + jobId,
					null,
					function(data) {
						$("#jobTable").ioriGridReload();
						Iori.UI.showTip($i18n("iori.job.label.pauseSuccess"), "success");
					}
				);
			}
		});
	}
	
	// resume Job
	page.resumeJob = function(jobId) {
		Iori.UI.confirm($i18n("iori.job.label.resumeConfirm"), function(flag) {
			if(flag == true) {
				Iori.doPost($globalVO.config.bgApp.Clocky + "/services/job/resume/" + jobId,
					null,
					function(data) {
						$("#jobTable").ioriGridReload();
						Iori.UI.showTip($i18n("iori.job.label.resumeSuccess"), "success");
					}
				);
			}
		});
	}
	
	// trigger Job
	page.triggerJob = function(jobId) {
		Iori.UI.confirm($i18n("iori.job.label.triggerConfirm"), function(flag) {
			if(flag == true) {
				Iori.doPost($globalVO.config.bgApp.Clocky + "/services/job/trigger/" + jobId,
					null,
					function(data) {
						$("#jobTable").ioriGridReload();
						Iori.UI.showTip($i18n("iori.job.label.triggerSuccess"), "success");
					}
				);
			}
		});
	}
	
	// 查看Job执行记录
	page.viewJobTrace = function(jobId) {
		// 通过全局变量来传递参数
		Iori.jobId = jobId;
		Iori.UI.Modal.open("ioriframework/boot/job/jobTraceList.html");
	}
	
});

