Iori.Module.define("Iori.Job.Create", function(page, $S) {
	
	page.ready = function() {
		
	}
	
	page.submit = function() {
		var formObj = $("#createJobForm");
		if (formObj.ioriValidate()) {
			Iori.doPost($globalVO.config.bgApp.Clocky + "/services/job/create",
				formObj.formData(),
				function(flag) {
					if(flag == 1) {
						// 关闭创建Job窗口
						Iori.UI.Modal.close();
						Iori.UI.showTip($i18n("iori.job.label.addSuccess"), "success");
						// 重新查询Job列表
						Iori.Job.List.searchJob();
					} else {
						Iori.UI.Alert.error($i18n("iori.job.label.exists"), $("#createJobMsg"));
					}
				}
			);
		}
	}
	
});


